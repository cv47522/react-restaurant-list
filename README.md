# Summer 2020 Internships - Engineering Pre-assignment

## Frontend task - displaying a list of restaurants with sort functionality

### Implementations: React.js + Webpack + React Router
1. React.js: Component based programming.
2. Webpack: Bundle and compress modules.
3. React Router: Routing multiple restaurant introduction pages with different URLs.

## Instruction
1. Download and unzip the file.
2. Open terminal, cd to the folder and install its dependencies.
```
    cd react-restaurant-list
    npm i
```
3. Start webpack server and list to port 7070.
```
    npm run start
```
![server.png](https://imgur.com/SRm6lKL.png)

p.s. My latest GitLab project repo: https://gitlab.com/cv47522/react-restaurant-list

## Features
1. Responsive
![responsive-2.png](https://imgur.com/IVntAHT.png)
![responsive-1.png](https://imgur.com/lPZsmwp.png)

2. Sorting restaurants alphabetically
