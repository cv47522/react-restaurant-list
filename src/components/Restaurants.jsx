import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Blurhash } from 'react-blurhash';

import {
  CardImg,
  CardBody,
  CardSubtitle,
  Card,
  Button,
  CardTitle,
  CardText,
  Row,
  Col,
  Container }
from 'reactstrap';

export default class Restaurants extends React.Component{
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Container className='home-container my-5'>
          <Row className='my-3'>
            <Col>
              <Button color='primary'
                onClick={() => this.props.sortBy('name')}>
                  sort restaurants alphabetically
              </Button>
            </Col>
          </Row>
          <Row>
            {this.props.datas.map(data => {
              return (
                <Col md="4" sm="6" xs="12" className='my-2' key={data.name}>
                  <Card>
                    {/* <Blurhash className={this.state.showBlur ? '' : 'hidden'}
                      hash={data.blurhash}
                      width='100%'
                      height={200}
                      resolutionX={32}
                      resolutionY={32}
                      punch={1}
                     /> */}
                    <CardImg top width='80%' className='home-card fit-image' src={data.image} alt={data.name} />


                    <CardBody className='cardbody-bg'>
                      <CardText className='text-black-50 font-weight-light text-right my-0'>
                        <em><small>{data.city}</small></em>
                      </CardText>
                      <CardTitle className='h5 text-primary'>{data.name}</CardTitle>
                      <CardSubtitle className='h5'>{data.currency} {data.delivery_price}</CardSubtitle>
                      <CardText className='text-black-50'>{data.description}</CardText>
                      <Button className='btn-warning' tag={Link} to={`/restaurant/${data.name}`}>More</Button>
                    </CardBody>
                  </Card>
                </Col>
              );

               // console.log(data);
            })}

            </Row>
          </Container>
        </div>
    );
  }
}
