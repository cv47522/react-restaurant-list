import React, { useState } from 'react';
import datas from 'api/restaurants.json';
import Restaurants from 'components/Restaurants.jsx';
import RestaurantDetail from 'components/RestaurantDetail.jsx';
import './Main.css';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';

import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';

export default class Main extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      navbarToggle: false,
      datas: datas,
      direction: {
        name: 'asc',
      },
    };

    this.handleNavbarToggle = this.handleNavbarToggle.bind(this);
    this.sortBy = this.sortBy.bind(this);
  }

  sortBy(key) {
    this.setState({
      datas: datas.sort((a, b) => {
        let x = a[key];
        let y = b[key];

        return this.state.direction[key] === 'asc' && x > y
        ? 1
        : -1;
      }),
      direction: {
        [key]: this.state.direction[key] === 'asc'
        ? 'desc'
        : 'asc',
      },
    });
    console.log(this.state.direction);
  }

  render() {
    return (
      <Router>
        <div className='bg-faded'>
          <div className='shadow-lg mb-2 rounded'>
            <Navbar color='dark' dark expand='md'>
              <NavbarBrand className='text-warning' tag={Link} to='/'>
                Wolt Restaurant App 2020
              </NavbarBrand>
              <NavbarToggler onClick={this.handleNavbarToggle} />
              <Collapse isOpen={this.state.navbarToggle} navbar>
                <Nav className='mx-auto' navbar>
                  <NavItem className='mx-lg-3'>
                    <NavLink tag={Link} to='/'>Home</NavLink>
                  </NavItem>

                  <UncontrolledDropdown nav inNavbar className='mx-lg-3'>
                    <DropdownToggle nav caret>
                      Restaurants
                    </DropdownToggle>
                    <DropdownMenu right>
                      {this.state.datas.map(data => (
                        <DropdownItem key={data.name} tag={Link} to={`/restaurant/${data.name}`}>
                          {data.name}
                        </DropdownItem>
                      ))}
                    </DropdownMenu>
                  </UncontrolledDropdown>

                  <NavItem className='mx-lg-3'>
                    <NavLink target='_blank' href='https://gitlab.com/cv47522/react-restaurant-list'>
                    <i className="fa fa-gitlab" aria-hidden="true"></i>  GitLab</NavLink>
                  </NavItem>
                </Nav>

                <NavbarText>
                  <a  className='text-decoration-none text-secondary'
                    target='_blank' href='http://wantinghsieh.com'>
                    Wan-Ting Hsieh
                  </a>
                </NavbarText>
              </Collapse>
            </Navbar>
          </div>

          <Switch>
            <Route exact path='/'>
              <Restaurants datas={this.state.datas} sortBy={this.sortBy}/>
            </Route>
            {/* <Route exact path='/restaurant/:dataId' component={RestaurantDetail}/> */}
          </Switch>
        </div>
      </Router>
    );
  }

  handleNavbarToggle() {
    this.setState((prevState, props) => ({
      navbarToggle: !prevState.navbarToggle,
    }));
  }
}
